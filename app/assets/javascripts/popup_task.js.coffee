$ ->
  $modal = $('#modal')
  $modal_close = $modal.find('.close')
  $modal_container = $('#modal-container')
  $task_select_div = $('.activity_task_select')

  # Handle modal links with the data-remote attribute
  $('a[data-remote]').on 'ajax:success',  (xhr, data, status) ->
    $modal
      .html(data)
      .prepend($modal_close)
      .css('top', $(window).scrollTop() + 150)
      .show()
      
    $('form[data-remote]').on 'ajax:success',  (xhr, data, status) ->
      form_success data
      
      
    $('#modal .close').click ->
      $modal_container.hide()
      $modal.hide()
      false
      
    $modal_container.show();
    $.spin.imageBasePath = $('#baseurl').val() + '/assets/'
    $('.spin_input').spin()
    
  form_success = (data) ->
    if data.error.length == 0
      $modal_container.hide()
      $modal.hide()
      $task_select_div.html(data.html)
    else
      $modal.html(data.html)
      $('form[data-remote]').on 'ajax:success',  (xhr, data, status) ->
        form_success data