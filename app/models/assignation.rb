# == Schema Information
#
# Table name: assignations
#
#  id         :integer         not null, primary key
#  user_id    :integer
#  task_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Assignation < ActiveRecord::Base
  belongs_to :user
  belongs_to :task
  
  validates_presence_of :user_id
  validates_presence_of :task_id
end
