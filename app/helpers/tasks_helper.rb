module TasksHelper
  def form_or_remote_form object, *opts, &proc
    if request.xhr?
      form_for object, {:remote => true}, &proc
    else
      
      form_for object, *opts, &proc
    end
  end
end
