# == Schema Information
#
# Table name: time_logs
#
#  id            :integer         not null, primary key
#  daily_task_id :integer
#  start_time    :datetime
#  end_time      :datetime
#  created_at    :datetime
#  updated_at    :datetime
#

class TimeLog < ActiveRecord::Base
  belongs_to :daily_task
  
  
  def time_lapse
    end_buffer = Time.now
    if !self.end_time.nil?
      end_buffer = self.end_time
    end

    return (end_buffer - self.start_time).to_i
  end 
  
  
  def total_time_formatted
    time_seconds = time_lapse
    get_time(time_seconds)
  end
  
  def get_time(duration)
    [ (duration / 3600).to_i,  (duration / 60).to_i % 60, (duration % 60).to_i ].map{ |t| t.to_s.rjust(2, '0') }.join(':')
  end
    
end
