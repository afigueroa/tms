class OverviewController < ApplicationController
    before_filter :authenticate_user!
    before_filter :set_page_name
    load_and_authorize_resource :only => [:overview,:overview_department,:overview_staff]
    
    
    def set_page_name
      @page_name = "overview"
    end
    
    def overview
      @users = User.find(:all).select{|user| user.role? :staff}
    end
    
    def overview_department
      @department_id = current_user.department.id
      
      @users = User.find(:all, :conditions=>{:department_id => @department_id}).select{|user| user.role? :staff}
    end
    
    def overview_staff
      @current_user = current_user
      @tasks = User.find(current_user.id).tasks.uniq_by { |t| t.id }
      
    end
    
    def load_tasks
      @tasks = User.find(params[:user_id]).tasks.uniq_by { |t| t.id }
      render :partial => "tasks/overview_tasks", :locals => { :user_id => params[:user_id], :table_class => 'overview_tasks' }
    end
    
    #loading summary of a task
    def load_summary
      
      @task = Task.find(params[:task_id])
      @daily_task = get_daily_task(Date.today, params[:user_id])
      
      render :partial => "tasks/task_summary", :locals => { :user_id => params[:user_id] }
    end
    
    #loading task from staff view
    def load_task_info
       @task = Task.find(params[:task_id])
       @current_user = current_user
       @daily_task = create_or_get_daily_task(@current_user.id)
       
       render :partial => "tasks/task_summary", :locals => { :user_id => @current_user.id} 
    end
    
    def load_task_fill_form
      @task = Task.find(params[:task_id])
      @current_user = current_user
      date = Date.today
      @daily_task = DailyTask.find(:first, :conditions => { :user_id => @current_user.id, :task_id => @task.id, :date=> date })
      if @daily_task.nil?
        @daily_task = DailyTask.new
        @daily_task.state = 1
        @daily_task.date = date
        @daily_task.user_id = @current_user.id
        @daily_task.task_id = @task.id 
        @daily_task.save 
      end
      
      render :partial => "tasks/fill_task_form" 
    end
    
    
    def get_daily_task(date, user_id)
      dt = DailyTask.find(:first, :conditions => { :user_id => user_id, :task_id => @task.id, :date=>  date})
      dt
    end
    
    def create_or_get_daily_task(user_id)
      date = Date.today
      
      dt = get_daily_task(date, user_id)
      if dt.nil?
        dt = DailyTask.new
        dt.date = date
        dt.state = 1
        dt.user_id = user_id
        dt.task_id = @task.id  
      end
      dt
    end
    
    def create_and_fill_daily_task
      dt = create_or_get_daily_task(@current_user.id)
      
      dt.summary = params[:summary]
      dt.ammount = params[:ammount]
      dt
    end
    
    def log_time
      @task = Task.find(params[:task_id])
      @current_user = current_user
      
      if !params[:commit].nil?
        #store info button
        dt = create_and_fill_daily_task
        
        if dt.save
          render :json => {:notice=> 'Information saved', :error => ''}
        else
          render :json => {:notice=> '', :error => 'Error saving information'}
        end
        
      else
        
        
        
        if !params["Start"].nil?
          dt = create_and_fill_daily_task
          dt.state = 2
          #start button
          time_log = TimeLog.new
          time_log.start_time = DateTime.now
          
          error = false
          begin
            ActiveRecord::Base.transaction do
              dt.save!
              time_log.daily_task_id = dt.id
              time_log.save!
            end
          rescue ActiveRecord::RecordInvalid => invalid
            error = true
            error_list = ''
            time_log.errors.full_messages.each do |msg|
              error_list << msg
            end 
            render :json => {:notice=> '', :error => error_list}
          end
          
          if (!error)
            @daily_task = dt
            fill_info = render_to_string :partial => 'tasks/fill_task_form'
            summary_info = render_to_string :partial => 'tasks/task_summary', :locals => {:user_id => @current_user.id}
            render :json => {:notice=> 'Task started', :error => '', :html_form => fill_info, :html_summary => summary_info}
          end
    
        else
          if !params["Stop"].nil?
            if params[:summary].empty? || params[:ammount].empty?
              render :json => {:notice=> '', :error => 'Could not stop task. Summary and Quantity are required fields'}
            else
              #stop button
              dt = create_and_fill_daily_task
              dt.state = 3
              dt.time_logs.select{|tl| !tl.start_time.nil? && tl.end_time.nil?}.each do |unfinished_log|
                unfinished_log.end_time = DateTime.now
              end
              
              if (dt.save)
                @daily_task = dt
                fill_info = render_to_string :partial => 'tasks/fill_task_form'
                summary_info = render_to_string :partial => 'tasks/task_summary', :locals => {:user_id => @current_user.id}
                render :json => {:notice=> 'Task stopped', :error => '', :html_form => fill_info, :html_summary => summary_info}
              else
                error_list = ''
                time_log.errors.full_messages.each do |msg|
                  error_list << msg
                end 
                render :json => {:notice=> '', :error => error_list}
              end
            end

          end
        end
      end
    end
    
end
