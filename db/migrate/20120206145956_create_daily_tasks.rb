class CreateDailyTasks < ActiveRecord::Migration
  def change
    create_table :daily_tasks do |t|
      t.integer :user_id
      t.integer :task_id
      t.text :summary
      t.string :ammount
      t.integer :state

      t.timestamps
    end
  end
end
