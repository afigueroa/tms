class ReportController < ApplicationController
  before_filter :set_page_name
  def set_page_name
    @page_name = "reports"
  end

  def index
    @department_id = current_user.department.id
    @users = User.find(:all, :conditions=>{:department_id => @department_id}).select{|user| user.role? :staff}
  end

  def post

    report = params[:report]

    user_id = report[:user_id]

    from = report[:from]
    to = report[:to]

    department_id = current_user.department.id

    if(from != '')
    dateFrom = Date.strptime(from, '%m/%d/%Y')
    dateTimeFrom = DateTime.new(dateFrom.year,dateFrom.mon,dateFrom.mday,0,0,0)
    else
    dateTimeFrom = DateTime.new(1900,1,1,0,0,0)
    end

    if(to != '')
    dateTo = Date.strptime(to, '%m/%d/%Y')
    dateTimeTo = DateTime.new(dateTo.year,dateTo.mon,dateTo.mday,23,59,59)
    else
    dateTimeTo = DateTime.new(2100,12,31,23,59,59)
    end

    if(user_id != '')
      time_logs = TimeLog.find(:all, :include => [ :daily_task ], :conditions => ['daily_tasks.user_id = ? and start_time >= ? and start_time <= ?' , user_id,dateTimeFrom, dateTimeTo ])
    else
      time_logs = TimeLog.find(:all, :include => [ :daily_task,{:daily_task => :user} ], :conditions => ['users.department_id = ? and start_time >= ? and start_time <= ?' , department_id,dateTimeFrom, dateTimeTo ])
    end

    respond_to do |format|
      format.csv { export_timeLogs_csv(time_logs)}
    end

  end

  def export_timeLogs_csv(time_logs)

    require "csv"

    filename = I18n.l(Time.now, :format => :short) + "- TimeSheets.csv"

    csv_string = CSV.generate do |csv|

      csv << ["User Name","Task Name", "Start Time", "End Time", "Elapsed","Summary", "Quantity"]
      puts time_logs.to_json
      time_logs.each do |time_log|
        if (!time_log.daily_task.nil? && !time_log.daily_task.user.nil? && !time_log.daily_task.task.nil?)
          csv << [time_log.daily_task.user.name, time_log.daily_task.task.name, (time_log.start_time.nil? ? '': time_log.start_time.localtime.strftime('%d/%m/%Y %H:%M:%S')), (time_log.end_time.nil? ? '': time_log.end_time.localtime.strftime('%d/%m/%Y %H:%M:%S')), time_log.total_time_formatted, time_log.daily_task.summary, time_log.daily_task.ammount]
        end
      end

    end

    send_data csv_string, :filename => filename

  end

end
