class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :position
      t.string :phone_number
      t.references :department

      t.timestamps
    end
    add_index :users, :department_id
  end
end
