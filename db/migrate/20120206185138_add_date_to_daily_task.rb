class AddDateToDailyTask < ActiveRecord::Migration
  def change
    add_column :daily_tasks, :date, :date
  end
end
