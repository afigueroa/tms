class Ability
  include CanCan::Ability
 
  def initialize(user)
    user ||= User.new # guest user
    
    if user.role? :administrator
      can :manage, User
      can :manage, Organization
      can :manage, Department
      can :overview, Overview
      can :read, Role
    elsif user.role? :department_header
      can :manage, Task
      can :manage, Activity
      can :manage, TaskType
      can :manage, Assignation
      can :overview_department, Overview
    elsif user.role? :staff
      can :manage, DailyTask
      can :manage, TimeLog
      can :overview_staff, Overview
      # # manage products, assets he owns
      # can :manage, Product do |product|
        # product.try(:owner) == user
      # end
      # can :manage, Asset do |asset|
        # asset.assetable.try(:owner) == user
      # end
    end
  end
end