class PassthroughController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    
    path = root_path
    if current_user.role? :administrator
      path = overview_path
    else
      if current_user.role? :department_header
        path = overview_department_path
      else
        if current_user.role? :staff
          path = overview_staff_path
        end
      end
    end

    redirect_to path     
  end

end
