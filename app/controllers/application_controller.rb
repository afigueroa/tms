class ApplicationController < ActionController::Base
  before_filter :get_user
  before_filter :check_license
  
  protect_from_forgery
  layout :layout_by_resource
  protected

  def layout_by_resource
    if devise_controller?
      "authentication"
    else
      "application"
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    redirect_to root_url
  end
  
  
  def check_license
    if user_signed_in?
      if DateTime.now() > TMS::Application.config.license_date
        sign_out :user
        redirect_to new_user_session_path, alert: 'The trial period has expired'
      end
    end
      
    
    
    
  end
  
  # Make the current user object available to views
  #----------------------------------------
  def get_user
    @current_user = current_user
    # production @base_url = '/tms'
    # development @base_url = '/tms'
    @base_url = '/tms'
  end
  
  def render(*args)
    args.first[:layout] = false if request.xhr? and args.first[:layout].nil?
    super 
  end

  private
  

end
