class CreateAssignations < ActiveRecord::Migration
  def change
    create_table :assignations do |t|
      t.integer :user_id
      t.integer :task_id
      t.datetime :created_at

      t.timestamps
    end
  end
end
