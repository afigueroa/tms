class CreateTimeLogs < ActiveRecord::Migration
  def change
    create_table :time_logs do |t|
      t.integer :daily_task_id
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
