class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.string :name
      t.text :description
      t.references :organization

      t.timestamps
    end
    add_index :departments, :organization_id
  end
end
