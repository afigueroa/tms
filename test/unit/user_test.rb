# == Schema Information
#
# Table name: users
#
#  id                     :integer         not null, primary key
#  name                   :string(255)
#  position               :string(255)
#  phone_number           :string(255)
#  department_id          :integer
#  created_at             :datetime
#  updated_at             :datetime
#  email                  :string(255)     default(""), not null
#  encrypted_password     :string(128)     default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer         default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should create User" do
    user = User.new
    user.name = "User"
    user.position= "User pos."
    user.department = departments(:DepartmentOne)
    user.email = "onemail@mail.com"
    user.password = "123456"
    user.password_confirmation = "123456"
    assert user.save
  end

  test "should find User" do
    user_id = users(:UserOne).id
    assert_nothing_raised { User.find(user_id) }
  end
  
  test "should update User" do
    user = users(:UserOne)
    assert user.update_attributes(:name => 'New name', :position=> 'new pos', :email => 'changemail@mail.com')
  end
  
  test "should  destroyusers" do
    user = users(:UserOne)
    assert user.destroy
  end
end

