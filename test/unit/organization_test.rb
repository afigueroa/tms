# == Schema Information
#
# Table name: organizations
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  address    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'test_helper'

class OrganizationTest < ActiveSupport::TestCase
  test "should create Organization" do
    org = Organization.new
    org.name = "Organization"
    org.address= "Address1"
    assert org.save
  end

  test "should find Organization" do
    organization_id = organizations(:OrgOne).id
    assert_nothing_raised { Organization.find(organization_id) }
  end
  
  test "should update Organization" do
    org = organizations(:OrgOne)
    assert org.update_attributes(:name => 'New name', :address=> 'new address')
  end
  
  test "should not destroy organization with departments" do
    org = organizations(:OrgOne)
    assert !org.destroy
  end
  
end
