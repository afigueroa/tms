class TasksController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_page_name
  
  load_and_authorize_resource :only => [:show,:index,:new,:destroy,:edit,:create ,:update, :assign, :save_assign]
  
  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.find(:all, :conditions=>{:department_id => current_user.department.id})

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tasks }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @task = Task.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/new
  # GET /tasks/new.json
  def new
    @task = Task.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    @task = Task.find(params[:id])
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(params[:task])
    # ajax_flag = params[:ajax_flag]
    @task.department = Department.find(current_user.department.id)
    
    if request.xhr?
      
      if @task.save
        tasks = Task.find(:all, :conditions => {:department_id => current_user.department.id})
        task_list = render_to_string :partial => 'tasks', :locals => {:tasks => tasks}
        task_list =  task_list.html_safe.gsub(/\n/, '').gsub(/\t/, '').gsub(/\r/, '')
        render :json => {:html => task_list, :error => ''}
      else
        redraw_partial = render_to_string :action => 'new.html.erb', :layout => false
        redraw_partial = redraw_partial.html_safe.gsub(/\n/, '').gsub(/\t/, '').gsub(/\r/, '')
        render :json => {:html => redraw_partial, :error => 'Error'}
      end
    else
      respond_to do |format|
        if @task.save
              format.html { redirect_to @task, notice: 'Task was successfully created.' }
              format.json { render json: @task, status: :created, location: @task }  
        else
          format.html { render action: "new" }
          format.json { render json: @task.errors, status: :unprocessable_entity }
        end
      end
    end
    
      
    # else
      # if @task.save
        # render :json => {:status => "Error"}
        # # render :partial => 'tasks', :locals => {:tasks => current_user.department.tasks}
      # else
        # render :json => {:status => "Error"}
      # end
    # end
      
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @task = Task.find(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to tasks_url }
      format.json { head :ok }
    end
  end
  
  def set_page_name
    @page_name = "tasks"
  end
  
  def assign
    task_id = params[:task_id]
    @task = Task.find(task_id)
    @users = User.find(:all, :conditions => {:department_id => current_user.department.id}).select{|user| user.role? :staff}
    respond_to do |format|
      format.html # assign.html.erb
    end
  end
  
  def save_assign
    task_id = params[:task_id]
    params[:user_assigns] ||= []
    assigns = params[:user_assigns]
    
    if !task_id.nil?
      Assignation.delete_all(["task_id = ?", task_id])
    
      assigns.each do |assign|
        user = User.find(assign)
        if (!user.nil?)
          assignment = Assignation.new
          assignment.user_id = user.id
          assignment.task_id = task_id
          assignment.save
        end
      end
      
    end
    
    flash.now[:notice] = "The selected users have been assigned"
    
    @tasks = Task.find(:all, :conditions=>{:department_id => current_user.department.id})
    respond_to do |format|
      format.html { render action: "index" }
    end
  end
end
