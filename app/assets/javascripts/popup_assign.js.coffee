$ ->
  $modal = $('#modal')
  $modal_close = $modal.find('.close')
  $modal_container = $('#modal-container')
  
  # Handle modal links with the data-remote attribute
  $('a[data-remote]').on 'ajax:success',  (xhr, data, status) ->
    $modal
      .html(data)
      .prepend($modal_close)
      .css('top', $(window).scrollTop() + 150)
      .show()
          
    $('#modal .close').click ->
      $modal_container.hide()
      $modal.hide()
      false
      
    $modal_container.show();
