# == Schema Information
#
# Table name: departments
#
#  id              :integer         not null, primary key
#  name            :string(255)
#  description     :text
#  organization_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Department < ActiveRecord::Base
  belongs_to :organization
  validates_presence_of :name
  validates_presence_of :organization_id
  before_destroy :without_users 
  has_many :users
  has_many :tasks, :dependent => :destroy
  
  def to_s
    self.name
  end
  
  def without_users
    if self.users.size() > 0
      self.errors.add :base, "The department cannot be deleted because it has users associated to it."
    end
    self.errors.blank?
  end
end
