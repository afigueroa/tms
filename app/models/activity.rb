# == Schema Information
#
# Table name: activities
#
#  id          :integer         not null, primary key
#  name        :string(255)
#  description :text
#  task_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Activity < ActiveRecord::Base
  belongs_to :task
  validates_presence_of :name
  validates_presence_of :description
  validates_presence_of :task_id
  
  
  def to_s
    self.name
  end
end
