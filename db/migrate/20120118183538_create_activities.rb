class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.text :description
      t.references :task

      t.timestamps
    end
    add_index :activities, :task_id
  end
end
