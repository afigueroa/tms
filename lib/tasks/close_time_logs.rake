desc "Close time logs"
task :close_time_logs => :environment do
  # Your code goes here
  yesterday = Date.yesterday
  
  dateTimeFrom = DateTime.new(yesterday.year,yesterday.mon,yesterday.mday,0,0,0)
  yesterdayLastMinute = DateTime.new(yesterday.year,yesterday.mon,yesterday.mday,23,59,59)
  
  
  #State 2 => In progress
  @dailytasks = DailyTask.find(:all, :conditions => {:date=> yesterday, :state=> 2})
  @dailytasks.each do |dailytask|
    dailytask.time_logs.each do |time_log|
      if time_log.end_time.nil?
        time_log.end_time = yesterdayLastMinute
      end
    end
    dailytask.state = 3
    dailytask.save

  end
  
end