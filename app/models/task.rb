# == Schema Information
#
# Table name: tasks
#
#  id            :integer         not null, primary key
#  name          :string(255)
#  description   :text
#  max_time      :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  department_id :integer
#  task_type_id  :integer
#

class Task < ActiveRecord::Base
  
  validates_presence_of :name
  validates_presence_of :task_type_id
  validates_presence_of :department
  belongs_to :department
  belongs_to :task_type
  has_many :activities, :dependent => :destroy
  has_many :assignations, :dependent => :destroy
  has_many :users, :through => :assignations
  has_many :daily_tasks, :dependent => :destroy
  
  
  # gets the state of the task for the user
  # 1 -> Not started
  # 2 -> Started
  # 3 -> Stopped
  def get_state(user_id, date=Date.today)
    dt = DailyTask.find(:first, :conditions => { :user_id => user_id, :task_id => self.id, :date=> date })
    
    if dt.nil?
      return 1
    else
      puts "WILL REUTURN" + dt.to_json
      if dt.state.nil?
        return 1
      else
        dt.state  
      end
    end
  end
  
  def max_time_in_seconds
    if self.max_time.nil?
      return 0
    else
      time_splitted = self.max_time.split(':')
      if time_splitted.size == 1
        return time_splitted[0].to_i * 3600
      else
        if time_splitted.size == 2
          return time_splitted[0].to_i * 3600 + time_splitted[1].to_i * 60
        else
          return 0
        end
      end
    end
  end
  
  def to_s
    self.name
  end
end
