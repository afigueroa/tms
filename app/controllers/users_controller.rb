
class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :accessible_roles, :only => [:new, :edit, :show, :update, :create]
  before_filter :set_page_name
  
  load_and_authorize_resource :only => [:show,:index,:new,:destroy,:edit,:create ,:update]
  
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    puts "SHOW!!"
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    load_organizations
    if !@user.department.nil?
      @departments = Department.find(:all, :conditions => {:organization_id => @user.department.organization.id})
    else
      if !@selected_organization.nil?
        @departments = Department.find(:all, :conditions => {:organization_id => @selected_organization_id})
      else
        @departments = Array.new
      end
      
    end
    
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    load_organizations
    if !@user.department.nil?
      @departments = Department.find(:all, :conditions => {:organization_id => @user.department.organization.id})
    else
      if !@selected_organization.nil?
        @departments = Department.find(:all, :conditions => {:organization_id => @selected_organization_id})
      else
        @departments = Array.new
      end
      
    end
    
    
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    @user.role = params[:user][:role]
    load_organizations
    if !@user.department.nil?
      @departments = Department.find(:all, :conditions => {:organization_id => @user.department.organization.id})
    else
      if !@selected_organization.nil?
        @departments = Department.find(:all, :conditions => {:organization_id => @selected_organization_id})
      else
        @departments = Array.new
      end
      
    end
     

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    load_organizations
    if !@user.department.nil?
      @departments = Department.find(:all, :conditions => {:organization_id => @user.department.organization.id})
    else
      if !@selected_organization.nil?
        @departments = Department.find(:all, :conditions => {:organization_id => @selected_organization_id})
      else
        @departments = Array.new
      end
      
    end
    
    
    if params[:user][:password].blank?
      [:password,:password_confirmation].collect{|p| params[:user].delete(p) }
    end
    
    
 
    if !@user.department.nil?
      @departments = Department.find(:all, :conditions => {:organization_id => @user.department.organization.id})
    else
      @departments = Array.new
    end
    
    
    respond_to do |format|
      if @user.errors[:base].empty? and @user.update_attributes(params[:user])
        sign_in(@user, :bypass => true) if @user == current_user
        flash.now[:notice] = "The user has been updated"
        format.json { render :json => @user.to_json, :status => 200 }
        format.xml  { head :ok }
        format.html { render :action => :show }
      else
        format.json { render :text => "Could not update user", :status => :unprocessable_entity } #placeholder
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
        format.html { render :action => :edit, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    
    if !(@user.id == current_user.id)
      @current_user = current_user
      if @user.destroy
        respond_to do |format|
          format.html { redirect_to users_url }
          format.json { head :ok }
        end
      else
  
        error_list = ''
        @user.errors.full_messages.each do |msg|
          error_list << msg
        end 
        
        respond_to do |format|
          format.html { redirect_to users_url, :alert=>error_list }
          format.json { head :ok }
        end
      end
    else
      respond_to do |format|
          format.html { redirect_to users_url, :alert=>"You cannot delete the user that is currently logged in." }
          format.json { head :ok }
        end
    end
  end
  
 
    # Get roles accessible by the current user
  #----------------------------------------------------
  def accessible_roles
    @accessible_roles = Role.accessible_by(current_ability,:read)
  end
 

  #Ajax call.
  #Fill select boxes
  def get_departments
    val = params[:organization_id]
    #Use val to find records
    @departments = Department.find(:all, :conditions=>  { :organization_id => val })
    render :partial => "departments", :locals => { :departments => @departments }
  end
  
  def load_organizations
    
    @organizations = Organization.all
    organization = Organization.find(:first)
    
    if !@user.department.nil?
      @selected_organization = (@organizations.index @user.department.organization) 
      @selected_organization_id = @user.department.organization
    else
      @selected_organization = (@organizations.index organization) 
      @selected_organization_id = organization.id
    end
    
 
  end
  
  
  def set_page_name
    @page_name = "users"
  end
end
