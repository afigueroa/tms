class RemoveTaskTypeFromTasks < ActiveRecord::Migration
  def up
     remove_column :tasks, :task_type
  end

  def down
    add_column :tasks, :task_type, :string
  end
end
