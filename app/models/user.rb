# == Schema Information
#
# Table name: users
#
#  id                     :integer         not null, primary key
#  name                   :string(255)
#  position               :string(255)
#  phone_number           :string(255)
#  department_id          :integer
#  created_at             :datetime
#  updated_at             :datetime
#  email                  :string(255)     default(""), not null
#  encrypted_password     :string(128)     default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer         default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#

class User < ActiveRecord::Base
  has_and_belongs_to_many :roles
  has_many :assignations, :dependent => :destroy
  has_many :tasks, :through => :assignations
  validates_presence_of :name
  validates_presence_of :department_id, :if => :require_department 
  validate :maximum_numbers_of_admins 
  validate :maximum_numbers_of_department_head
  
  belongs_to :department
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :position, :department_id, :phone_number,  :role_ids, :role, :email, :password, :password_confirmation, :remember_me

  def role?(role_check)
    self.roles.each do |role|
      return true if (role.name.eql? role_check.to_s.humanize )
    end    
    
    return false
    #return !!self.roles.find_by_name(role_check.to_s.humanize)
  end
  
  def organization
    if !self.department.nil?
      self.department.organization
    end
  end
  
  def maximum_numbers_of_admins
    if self.role? :administrator
      puts self.id.to_s
      current_id = 0
      if !self.id.nil?
        current_id = self.id
      end
      count = User.find(:all, :joins=> [:roles], :conditions => [ "roles.name = ?  and users.id <> ? ", :administrator.to_s.humanize, current_id] ).count
      if count >= 2
        self.errors.add :base, "The system has already reached the maximum number of administrators"
      end
    end
  end
  
  def maximum_numbers_of_department_head

    if self.role? :department_header
      current_id = 0
      if !self.id.nil?
        current_id = self.id
      end
      current_dep_id = 0
      if (!self.department.nil?)
        current_dep_id = self.department.id;
      end
      count = User.find(:all, :joins=> [:roles], :conditions => [ "roles.name = ? and users.id <> ? and users.department_id = ?",:department_header.to_s.humanize ,current_id, current_dep_id] ).count
      
      # current_org_id = 0
      # if (!self.department.nil? && !self.department.organization.nil?)
        # current_org_id = self.department.organization.id
      # end
      # count = User.find(:all, :joins=> [:roles, :department], :conditions => [ "roles.name = ? and users.id <> ? and departments.organization_id = ?",:department_header.to_s.humanize ,current_id, current_org_id] ).count
       if count >= 3
        self.errors.add :base, "The department has already reached the maximum number of user in role Department Header"
      end
      
    end
  end
 
  def require_department
    return !(self.role? :administrator)
  end
  
  def role=(role_id)
    self.roles.clear
    puts role_id.to_s
    self.roles << Role.find(role_id)
    puts self.roles.to_s
  end
  
  def role
    self.roles.first unless self.roles.length == 0
  end
  
end
