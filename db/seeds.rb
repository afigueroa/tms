# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Role.create(name: "Administrator")
Role.create(name: "Department header")
Role.create(name: "Staff")

Organization.create(name: 'Organization 1', address: 'Org address')
Department.create(name: 'Department 1', description: 'description 1', organization_id: 1)

u1 = User.create(name: 'Admin', position: 'Test position', department_id: 1,  email: 'admin@mail.com', password: 'toptier', password_confirmation: 'toptier')
u1.roles.push Role.find(1)

u2 = User.create(name: 'John', position: 'Test position', department_id: 1, email: 'deptheader@mail.com', password: 'toptier', password_confirmation: 'toptier')
u2.roles.push Role.find(2)

u3 = User.create(name: 'Peter Staff', position: 'Developer', department_id: 1, email: 'staff@mail.com', password: 'toptier', password_confirmation: 'toptier')
u3.roles.push Role.find(3)

tt = TaskType.create(name: 'Type 1')
