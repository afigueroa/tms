# == Schema Information
#
# Table name: task_types
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class TaskType < ActiveRecord::Base
  validates_presence_of :name
  has_many :tasks
  before_destroy :without_tasks
  
  def to_s
    return self.name
  end
  
  def without_tasks

    if self.tasks.size() > 0
      self.errors.add :base, "The task type cannot be deleted because it has tasks associated to it."
    end

    self.errors.blank?
  end
end
