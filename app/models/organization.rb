# == Schema Information
#
# Table name: organizations
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  address    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Organization < ActiveRecord::Base
  has_many :departments
  validates_presence_of :name
  before_destroy :without_departments
  def to_s
    self.name
  end
  
  def without_departments
 
    if self.departments.size() > 0
      self.errors.add :base, "The organization cannot be deleted because it has departments associated to it."
    end

    self.errors.blank?
  end
end
