# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$ ->
  #When clicking on overview staff table get the tasks for that user and bind the event to that table
  $('.overview_staff td').click -> 
    $('.overview_staff td').removeClass('highlight')
    $(this).addClass('highlight')
    
    user_id = $(this).find('input[name="user_id"]').val()
    $.ajax $('#baseurl').val() + "/overview/load_tasks?user_id="+  user_id,
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
       
      success: (data, textStatus, jqXHR) ->
        $('#dashboard_tasks').html(data)
        $('#dashboard_summary').html('')
        $('.overview_tasks td').click ->
          $('.overview_tasks td').removeClass('highlight')
          $(this).addClass('highlight')
          #Load summary of the task
          $.ajax $('#baseurl').val() +  "/overview/load_summary?task_id="+ $(this).find('input[name="task_id"]').val() + "&user_id=" + $(this).find('input[name="user_id"]').val() ,
            dataType: 'html'
            error: (jqXHR, textStatus, errorThrown) ->
       
            success: (data, textStatus, jqXHR) ->
              $('#dashboard_summary').html(data)
        
  # Select a task
  $('.overview_staff_tasks td').click ->
    $('.overview_staff_tasks td').removeClass('highlight')
    $(this).addClass('highlight')
    $.ajax $('#baseurl').val() +  "/overview/load_task_info?task_id="+ $(this).find('input[name="task_id"]').val(),
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
       
      success: (data, textStatus, jqXHR) ->
        $('#task_summary').html(data)
        
    $.ajax $('#baseurl').val() + "/overview/load_task_fill_form?task_id="+ $(this).find('input[name="task_id"]').val(),
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
       
      success: (data, textStatus, jqXHR) ->
        $('#fill_task_form').html(data)
        # Handle modal links with the data-remote attribute
        $('#form_fill_task').on 'ajax:success',  (xhr, data, status) ->
          form_fill_callback(data)
          
  # Callback after pressing Start/Stop button        
  form_fill_callback = (data) ->
    $('p.notice').html(data.notice)
    $('p.alert').html(data.error)
    setTimeout ( ->
      $('p.alert').html('')
      $('p.notice').html('')
    ), 6000
    unless typeof data.html_form is "undefined"
      if data.html_form.length != 0
        $('#fill_task_form').html(data.html_form)
        $('#form_fill_task').on 'ajax:success',  (xhr, data, status) ->
          form_fill_callback(data)
    unless typeof data.html_summary is "undefined"
      if data.html_summary.length != 0
        $('#task_summary').html(data.html_summary)


