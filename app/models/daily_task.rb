# == Schema Information
#
# Table name: daily_tasks
#
#  id         :integer         not null, primary key
#  user_id    :integer
#  task_id    :integer
#  summary    :text
#  ammount    :string(255)
#  state      :integer
#  created_at :datetime
#  updated_at :datetime
#  date       :date
#

# States 1=Not started 2=In progress 3=Finished
class DailyTask < ActiveRecord::Base
    has_many :time_logs, :order => 'start_time', :dependent => :destroy
    accepts_nested_attributes_for :time_logs
    belongs_to :user
    belongs_to :task
    
    
    def total_time
      total = 0
      self.time_logs.each do |time_log|
        total = total + time_log.time_lapse
      end
      total
    end
    
    def total_time_formatted
      time_seconds = total_time
      get_time(time_seconds)
    end
    
    def get_time(duration)
      [ (duration / 3600).to_i,  (duration / 60).to_i % 60, (duration % 60).to_i ].map{ |t| t.to_s.rjust(2, '0') }.join(':')
    end
    
    
end
