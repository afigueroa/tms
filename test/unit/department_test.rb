# == Schema Information
#
# Table name: departments
#
#  id              :integer         not null, primary key
#  name            :string(255)
#  description     :text
#  organization_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

require 'test_helper'

class DepartmentTest < ActiveSupport::TestCase
  test "should create Department" do
    dep = Department.new
    dep.name = "Department"
    dep.description= "Dep desc"
    dep.organization = organizations(:OrgOne)
    assert dep.save
  end

  test "should find Department" do
    department_id = departments(:DepartmentOne).id
    assert_nothing_raised { Department.find(department_id) }
  end
  
  test "should update Department" do
    dep = departments(:DepartmentOne)
    assert dep.update_attributes(:name => 'New name', :description=> 'new desc')
  end
  
  test "should not destroy department with users" do
    dep = departments(:DepartmentOne)
    assert !dep.destroy
  end
  
end
