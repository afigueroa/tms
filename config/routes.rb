TMS::Application.routes.draw do
  resources :task_types

  resources :activities

  resources :tasks

  devise_for :users

  resources :users

  resources :departments
  resources :roles
  resources :organizations

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  
  root :controller=>'passthrough', :action => 'index'
  # replace devise_for :users with:
  devise_for :users,  :controllers => { :registrations => "users/registrations" }
  match 'get_departments', :controller=>'users', :action => 'get_departments'
  match 'overview', :controller=>'overview', :action => 'overview', :as => "overview"
  match 'overview/load_tasks', :controller=>'overview', :action => 'load_tasks', :as => "load_tasks"
  match 'overview/load_task_info', :controller=>'overview', :action => 'load_task_info', :as => "load_task_info"
  match 'overview/load_task_fill_form', :controller=>'overview', :action => 'load_task_fill_form', :as => "load_task_fill_form"
  match 'overview/load_summary', :controller=>'overview', :action => 'load_summary', :as => "load_summary"
  match 'report', :controller=>'report', :action => 'index', :as => "report"
  match 'tasks/assign_task/:task_id', :controller=>'tasks', :action => 'assign', :as => "assign_task"
  match 'tasks/save_assign', :controller=>'tasks', :action => 'save_assign', :as => "save_assign"
  match 'overview_department', :controller=>'overview', :action => 'overview_department', :as => "overview_department"
  match 'overview_staff', :controller=>'overview', :action => 'overview_staff', :as => "overview_staff"
  match 'log_time', :controller=>'overview', :action=>'log_time', :as => 'log_time'
  match 'report/post', :controller=>'report', :action=>'post', :as => 'report_csv'
  match 'report/error', :controller=>'report', :action=>'error'
end
