class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.string :max_time
      t.string :task_type

      t.timestamps
    end
  end
end
