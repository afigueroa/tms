class DepartmentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_page_name
  
  # Devise authorize actions
  load_and_authorize_resource :only => [:show,:index,:new,:destroy,:edit,:create ,:update]
  
  # GET /departments
  # GET /departments.json
  def index
    @departments = Department.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @departments }
    end
  end

  # GET /departments/1
  # GET /departments/1.json
  def show
    @department = Department.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @department }
    end
    
  end

  # GET /departments/new
  # GET /departments/new.json
  def new
    @department = Department.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @department }
    end
  end

  # GET /departments/1/edit
  def edit
    @department = Department.find(params[:id])
  end

  # POST /departments
  # POST /departments.json
  def create
    @department = Department.new(params[:department])

    respond_to do |format|
      if @department.save
        format.html { redirect_to @department, notice: 'Department was successfully created.' }
        format.json { render json: @department, status: :created, location: @department }
      else
        format.html { render action: "new" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /departments/1
  # PUT /departments/1.json
  def update
    @department = Department.find(params[:id])

    respond_to do |format|
      if @department.update_attributes(params[:department])
        format.html { redirect_to @department, notice: 'Department was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @department = Department.find(params[:id])
    if @department.destroy
      respond_to do |format|
        format.html { redirect_to departments_url }
        format.json { head :ok }
      end
    else

      error_list = ''
      @department.errors.full_messages.each do |msg|
        error_list << msg
      end 
      
      respond_to do |format|
        format.html { redirect_to departments_url, :alert=>error_list }
        format.json { head :error }
      end
    end
  end
  
  def set_page_name
    @page_name = "departments"
  end

 
end
